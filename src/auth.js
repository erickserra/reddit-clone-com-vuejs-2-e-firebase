/* eslint-disable */
import firebase from '@/firebase';
import store from '@/store/index';
import db from '@/db';
import router from '@/router';

firebase.auth().onAuthStateChanged(function(user) {
  
  if (user) {
  
    if (user.user) {
      user = user.user
    } 

    const firebaseUser = {
			id: user.uid,
			name: user.displayName,
			image: user.photoURL,
			created_at: firebase.firestore.FieldValue.serverTimestamp(),
		};

		db.collection('users').doc(firebaseUser.id).set(firebaseUser);

    store.commit('auth/setUser', firebaseUser);
    router.push('/subreddits');
    
  } else {
    store.commit('auth/setUser', null);
    router.push('/');
  }

  store.commit('loading/setLoader', false);
});
