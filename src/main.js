/* eslint-disable */
import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import './assets/scss/variables.scss'
import App from './App.vue';
import router from './router';
import store from './store';
import auth from './auth';

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

