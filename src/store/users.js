/* eslint-disable */
import { firestoreAction } from 'vuexfire';
import db from '@/db';

const state = {
  users: [],
};

const mutations = {

};

const actions = {
  init: firestoreAction(({ bindFirestoreRef }) =>
    // return the promise returned by `bindFirestoreRef`
    bindFirestoreRef('users', db.collection('users'))),
};

const getters = {
  usersById(state) {
    return state.users.reduce((byId, user) => {
      byId[user.id] = user;
      return byId;
    }, {});
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
