/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';
import { vuexfireMutations } from 'vuexfire'
import auth from '@/store/auth';
import loading from '@/store/loading';
import subreddits from '@/store/subreddits';
import subreddit from '@/store/subreddit';
import users from '@/store/users';

Vue.use(Vuex);

export default new Vuex.Store({
  mutations: vuexfireMutations,
  modules: {
    auth,
    loading,
    subreddits,
    subreddit,
    users
  },
});
