/* eslint-disable */
import { firestoreAction } from 'vuexfire';
import firebase from '@/firebase';
import  db  from '@/db'
import loading from './loading';

const posts = db.collection('posts');

const state = {
  subreddits: [],
  posts: [],
};

const actions = {
  async createPost ({ state, commit }, post) {

    loading.state.mainLoaderActive = true;
    const result = posts.doc();

    post.id = result.id;
    post.subreddit_id = state.subreddits[0].id,
    post.user_id = firebase.auth().currentUser.uid;
    post.created_at = firebase.firestore.FieldValue.serverTimestamp();
    post.created_at = firebase.firestore.FieldValue.serverTimestamp();

    await posts.doc(post.id).set(post);
    
    loading.state.mainLoaderActive = false;
  },
  initSubreddit: firestoreAction(({ bindFirestoreRef }, { name }) => {
    return bindFirestoreRef('subreddits', db.collection('subreddits').where('name', '==', name));
  }),
  initPosts: firestoreAction(({ bindFirestoreRef }, { subredditId }) => {
    return bindFirestoreRef('posts', db.collection('posts').where('subreddit_id', '==', subredditId));
  }),
  async deletePost ({ rootState }, postId) {
    rootState.loading.mainLoaderActive = true;
    try {
      await posts.doc(postId).delete();
      rootState.loading.mainLoaderActive = false;
    } catch (error) {
      rootState.loading.mainLoaderActive = false; 
    }  
  }
};

const getters = {
  subreddit(state) {
    return state.subreddits[0] ? state.subreddits[0] : {};
  }
};

export default {
  namespaced: true,
  state,
  actions,
  getters
}