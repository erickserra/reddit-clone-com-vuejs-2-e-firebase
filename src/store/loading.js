/* eslint-disable */
const state = {
  mainLoaderActive: true
}

const mutations = {
  setLoader(state, payload) {
    state.mainLoaderActive = payload;
  }
}

export default {
  namespaced: true,
  state,
  mutations
}